import Vue from 'vue'
import clock from './components/ClockManager';
import WorkingTimes from './components/WorkingTimes'
import WorkingTime from './components/WorkingTime'
import ChartManager from './components/ChartManager'
import Router from 'vue-router'

Vue.use(Router)

export default new Router(
    {
        routes: [
            {   path:'/clock/:username',
                name: 'clock',
                component:clock
            },

            {
                path: '/workingTimes/:userID',
                name: 'WorkingTimes',
                component: WorkingTimes
            },

            {
                path: '/workingTime/:userid',
                name: 'WorkingTime',
                component: WorkingTime
            },

            {
                path: '/chartManager/:userid',
                name: 'chartManager',
                component: ChartManager
            },

            {
                path: '/workingTime/:userid/:workingtimeid',
                name: 'WorkingTime',
                component: WorkingTime
            },
        ]
    }
)