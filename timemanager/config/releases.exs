import Config

secret_key_base = System.fetch_env!("SECRET_KEY_BASE")
app_port = System.fetch_env!("APP_PORT")
app_hostname = System.fetch_env!("APP_HOSTNAME")
db_user = System.fetch_env!("DB_USER")
db_password = System.fetch_env!("DB_PASSWORD")
db_host = System.fetch_env!("DB_HOST")

config :timemanager, TimemanagerWeb.Endpoint,
  http: [:inet6, port: String.to_integer(app_port)],
  secret_key_base: secret_key_base

config :timemanager,
  app_port: app_port

config :timemanager,
  app_hostname: app_hostname

# Configure your database
config :timemanager, Timemanager.Repo,
  username: "postgres",
  password: "postgres",
  database: "timemanager_dev",
  hostname: "localhost",
  pool_size: 10
