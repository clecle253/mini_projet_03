defmodule TimemanagerWeb.ClockController do
  use TimemanagerWeb, :controller

  alias Timemanager.ClockTime
  alias Timemanager.ClockTime.Clock

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, _params) do
    clocks = ClockTime.list_clocks()
    render(conn, "index.json", clocks: clocks)
  end

  def create(conn, %{"userID" => userID}) do
    status = true
    time = DateTime.utc_now
    clock_params = %{"time" => time, "status" => status, "user" => userID}
    with {:ok, %Clock{} = clock} <- ClockTime.create_clock(clock_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.clock_path(conn, :get_clock_from_userID, clock))
      |> render("show.json", clock: clock)
    end
  end

  def show(conn, %{"clockID" => clockID}) do
    clock = ClockTime.get_clock!(clockID)
    render(conn, "show.json", clock: clock)
  end

  def update(conn, %{"clockID" => clockID, "clock" => clock_params}) do
    clock = ClockTime.get_clock!(clockID)

    with {:ok, %Clock{} = clock} <- ClockTime.update_clock(clock, clock_params) do
      render(conn, "show.json", clock: clock)
    end
  end

  def delete(conn, %{"clockID" => clockID}) do
    clock = ClockTime.get_clock!(clockID)

    with {:ok, %Clock{}} <- ClockTime.delete_clock(clock) do
      send_resp(conn, :no_content, "")
    end
  end

  def get_clock_from_userID(conn, %{"userID" => userID}) do
    clock = ClockTime.get_from_user(userID)
    render(conn, "show.json", clock: clock)
  end

end
