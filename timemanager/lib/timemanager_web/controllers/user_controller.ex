defmodule TimemanagerWeb.UserController do
  use TimemanagerWeb, :controller

  alias Timemanager.Account
  alias Timemanager.Account.User

  action_fallback TimemanagerWeb.FallbackController

  def index(conn, _params) do
    users = Account.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Account.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"userID" => userID}) do
    user = Account.get_user!(userID)
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"userID" => userID, "user" => user_params}) do
    user = Account.get_user!(userID)

    with {:ok, %User{} = user} <- Account.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"userID" => userID}) do
    user = Account.get_user!(userID)

    with {:ok, %User{}} <- Account.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end

  def get_user_from_email(conn, %{"email" => email, "username" => username}) do
    user = Account.get_user_from_email_username(email, username)
    render(conn, "show.json", user: user)
  end
end
