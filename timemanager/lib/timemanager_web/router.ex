defmodule TimemanagerWeb.Router do
  use TimemanagerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  scope "/", TimemanagerWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", TimemanagerWeb do
    pipe_through :api

    get "/users/list", UserController, :index
    get "/users", UserController, :get_user_from_email
    get "/users/:userID", UserController, :show
    post "/users", UserController, :create
    put "/users/:userID", UserController, :update
    delete "/users/:userID", UserController, :delete

    get "/clocks/:userID", ClockController, :get_clock_from_userID
    post "/clocks/:userID", ClockController, :create
    
    resources "/workingtimes", WorkingtimeController, except: [:new, :edit]
  end
end
